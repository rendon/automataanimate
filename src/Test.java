/*
    Copyright (C) 2013 Rafael Rendón Pablo <smart.rendon@gmail.com>
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import edu.inforscience.graphics.Dimension;
import edu.inforscience.graphics.Plane;

import javax.swing.*;
import java.awt.*;

public class Test extends JFrame implements Runnable {
  private Plane plane;
  public Test()
  {
    super("Testing Automata Animation");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setSize(1000, 800);
    setVisible(true);
    setLocationRelativeTo(null);
    plane = new Plane();
    add(plane);
  }
  public static void main(String[] args)
  {
    Test test = new Test();
    Thread thread = new Thread(test);
    thread.start();
  }

  @Override
  public void run()
  {
    plane.addState(new Dimension(0, 4, 1), "1", true, false);
    plane.addState(new Dimension(0, 0, 1), "2", false, false);
    plane.addState(new Dimension(10, 10, 1), "3", false, true);
    plane.addEdge(0, 2, "[Aa-Za]");
    plane.addEdge(0, 1, "[_, 0-9]");
    plane.addEdge(2, 2, "[Aa-Zz, 0-9, _]");
    plane.addEdge(2, 0, "[Aa-Zz]");

    animateState(0, Color.RED);
    pause(1000);
    resetState(0);

    animateEdge(0, 1, Color.BLUE);
    pause(1000);
    resetEdge(0, 1);

    animateState(1, Color.RED);
    pause(1000);
    resetState(1);

    animateState(0, Color.RED);
    pause(1000);
    resetState(0);

    animateEdge(0, 2, Color.BLUE);
    pause(1000);
    resetEdge(0, 2);

    animateState(2, Color.RED);
    pause(1000);
    resetState(2);
  }

  public void animateState(int index, Color color)
  {
    plane.setVertexColor(index, color);
    pause(1000);
    resetState(index);
  }

  public void resetState(int index)
  {
    //White is the default color
    plane.setVertexColor(index, Color.WHITE);
    repaint();
  }

  public void animateEdge(int u, int v, Color color)
  {
    plane.setEdgeColor(u, v, color);
    pause(1000);
    resetEdge(u, v);
  }// End of animateEdge()

  public void resetEdge(int u, int v)
  {
    // Black is the default color for edges
    plane.setEdgeColor(u, v, Color.BLACK);
  }

  public void pause(int seconds)
  {
    try {
      Thread.sleep(seconds);
    } catch( InterruptedException ie ) {
    }
  }
}
